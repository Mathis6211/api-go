# Start from the base Golang image
FROM golang:1.16

# Work repositery
WORKDIR /app

# Copy files go.mod and go.sum
COPY go.mod go.sum ./

# Download dependencies
RUN go mod download

# Copy the source code in the container
COPY . .

# Compile the application
RUN go build -o main .

# Expose the application Port
EXPOSE 59362

# Start the application
CMD ["/app/main"]
