package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

//Function to setup the router to test the routes
func setupRouter() *gin.Engine {
	router := gin.Default()

	router.GET("/gdocuments", getDocuments)
	router.POST("/cdocuments", createDocument)
	router.GET("/documents/:id", documentByID)
	router.DELETE("/ddocuments/:id", deleteDocument)

	return router
}

//function to test GetDocuments
func TestGetDocuments(t *testing.T) {
	gin.SetMode(gin.TestMode)
	router := setupRouter()

	req, err := http.NewRequest("GET", "/gdocuments", nil)
	if err != nil {
		t.Fatalf("Couldn't create request: %v\n", err)
	}
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)
}

//function to test CreateDocument
func TestCreateDocument(t *testing.T) {
	gin.SetMode(gin.TestMode)
	router := setupRouter()

	newDoc := &document{
		ID:          "5",
		Name:        "Test Name",
		Description: "Test Description",
	}
	jsonDoc, _ := json.Marshal(newDoc)
	req, err := http.NewRequest("POST", "/cdocuments", strings.NewReader(string(jsonDoc)))
	if err != nil {
		t.Fatalf("Couldn't create request: %v\n", err)
	}
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	var responseDoc document
	err = json.Unmarshal(resp.Body.Bytes(), &responseDoc)
	assert.Nil(t, err)
	assert.Equal(t, *newDoc, responseDoc)
}

//function to test DocumentByID
func TestDocumentByID(t *testing.T) {
	gin.SetMode(gin.TestMode)
	router := setupRouter()

	id := "1"

	req, err := http.NewRequest("GET", "/documents/"+id, nil)
	if err != nil {
		t.Fatalf("Couldn't create request: %v\n", err)
	}
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)

	var responseDoc document
	err = json.Unmarshal(resp.Body.Bytes(), &responseDoc)
	assert.Nil(t, err)

	expectedDoc, _ := getDocumentByID(id)
	assert.Equal(t, *expectedDoc, responseDoc)
}

//function to test DeleteDocument
func TestDeleteDocument(t *testing.T) {
	gin.SetMode(gin.TestMode)
	router := setupRouter()

	id := "1"

	req, err := http.NewRequest("DELETE", "/ddocuments/"+id, nil)
	if err != nil {
		t.Fatalf("Couldn't create request: %v\n", err)
	}
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)

	_, err = getDocumentByID(id)
	assert.NotNil(t, err)
	assert.Equal(t, "document not found", err.Error())
}