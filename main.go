package main

import (
	"errors"
	"fmt"
	"net"
	"net/http"

	"github.com/gin-gonic/gin"
)

type document struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

var documents = []document{
	{ID: "1", Name: "123", Description: "Counting could be funny"},
	{ID: "2", Name: "Surdently", Description: "I don't think so"},
	{ID: "3", Name: "Can u repeat", Description: "Can u repeat, canu repeat"},
	{ID: "4", Name: "I was sure about that", Description: "This is confidence"},
}

func getDocuments(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, documents)
}

func documentByID(c *gin.Context) {
	id := c.Param("id")
	document, err := getDocumentByID(id)

	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "document not found"})
		return
	}

	c.IndentedJSON(http.StatusOK, document)
}

func getDocumentByID(id string) (*document, error) {

	for i, d := range documents {
		if d.ID == id {
			return &documents[i], nil
		}
	}

	return nil, errors.New("document not found")
}

func createDocument(c *gin.Context) {
	var newDocument document

	if err := c.BindJSON(&newDocument); err != nil {
		return
	}

	documents = append(documents, newDocument)
	c.IndentedJSON(http.StatusCreated, newDocument)
}

func deleteDocument(c *gin.Context) {
	id := c.Param("id")
	_, err := getDocumentByID(id)

	if err != nil {
		fmt.Println("this document doesn't exist", err)
		return
	}

	for i, d := range documents {
		if d.ID == id {
			documents = append(documents[:i], documents[i+1:]...)
			break
		}
	}

	c.IndentedJSON(http.StatusOK, gin.H{"message": "document deleted"})
}

func findFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}
	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer listener.Close()
	return listener.Addr().(*net.TCPAddr).Port, nil
}

func main() {

	port, err := findFreePort()
	if err != nil {
		fmt.Println("Erreur lors de la recherche du port disponible :", err)
		return
	}
	fmt.Println("Port disponible :", port)

	url := fmt.Sprintf("http://localhost:%d", port)
	fmt.Println("URL :", url)

	router := gin.Default()

	router.GET("/gdocuments", getDocuments)
	router.POST("/cdocuments", createDocument)
	router.GET("/documents/:id", documentByID)
	router.DELETE("/ddocuments/:id", deleteDocument)

	router.Run("localhost:59362")
}
