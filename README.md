## First commands to test manually ##

## To test getBooks
curl localhost:59362/gdocuments

## To test createdocuments avec le fichier Document.json
curl localhost:59362/cdocuments --include --header "Content-Type: application/json" -d @document.json --request "POST"

ou

curl -X POST -H "Content-Type: application/json" -d @document.json http://localhost:59362/cdocuments


## To test DocumentkByID
curl localhost:59362/documents/3

## To test with test_main.go ##
go test

## To test with Docker ##

(I have installed Docker Desktop)

# To build a Docker image 
docker build -t api-go .

# Now to compile the application
go build -o main